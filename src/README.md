# Binaries

## agent

Our secret agent fetches the websites of all the database's packages and checks their contents. Then it updates the information contained in the database. It adds data about the newest version, its size, the download path and the release date. This program works without user interaction or observation and it is able to run from a cron job or a task.

Settings:

Parameters:


## fetcher

The fetcher downloads the selected current installation files. It cleans old files and adds the checksum to the database. This program works without user interaction or observation and it is able to run from a cron job or a task.

Settings:

Parameters:


## brewer

The brewer builds uniformly formatted archives out of the downloaded single setup files. It creates an archive with product name and version in its name. Then it copies this archive into the repository of installable packages and cleans up old archives. This program works without user interaction or observation and it is able to run from a cron job or a task.

Settings:

Parameters:


## dealer

The dealer is your local candy shop. He creates a list of known products and gives the user the opportunity, to try them out, to just install them, or to subscribe them permanently.

Settings:

Parameters:


## courier

The courier ist the end-user distributor. You will receive the latest version for each subscribed application. These are installed or updated without interruption or further notification. Old installations are silently removed. The courier installs applications using a subscription list with needed applications or the currently existing
list with installed applications. It uses a repository containing archived applications. Applications may get installed with or without an version number in the directory name. This program works without user interaction or observation and it is able to run from a cron job or a task.

Settings:

  never-remove-old-versions
  install-and-remove-old-version-after-x-days
  update-without-interruption
  always-check-existing-versions

Parameters:

  command         - install, remove, check, update

  repository-dir  - Path to the repository containing the archived applications,
                    optionally a list of host names/IP numbers and needed apps.

  target-dir      - directory with the installed applications.

  without-version - don't add a version number to the application's directory name.

  app-name(s)     - list of applications to be processed. If omitted the current
                    application subscription or determined list will be used.


# How to build:

mkdir build
cd build
cmake ..
ninja
