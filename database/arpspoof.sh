#!/bin/bash

POAPPIN_NAME="arpspoof"
POAPPIN_DESCRIPTION="A simple ARP spoofer for Windows"
POAPPIN_HOMEPAGE="https://github.com/alandau/arpspoof/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# Needs Wireshark or Winpcap (https://www.winpcap.org/).

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/arpspoof-\([0-9].*\)\.exe/\1/p'`
  # 0.1
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://github.com/alandau/arpspoof/releases/latest"`

  POAPPIN_URL="https://github.com"`echo "${DATA}" | sed -n 's/.*<a href="\(\/alandau\/.*\/arpspoof\.exe\)" rel=.*>/\1/p'`
  # https://github.com/alandau/arpspoof/releases/download/v0.1/arpspoof.exe

  POAPPIN_VERSION=`echo "${POAPPIN_URL}" | sed -n 's/.*\/download\/v\([0-9].*\)\/arpspoof.*/\1/p'`
  # 0.1

  POAPPIN_FILENAME="${POAPPIN_NAME}-${POAPPIN_VERSION}.exe"
  # arpspoof-0.1.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  cp "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" arpspoof.exe
}
