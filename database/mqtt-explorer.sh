#!/bin/bash

POAPPIN_NAME="mqtt-explorer"
POAPPIN_DESCRIPTION="MQTT client that provides a structured overview of your MQTT topics"
POAPPIN_HOMEPAGE="https://mqtt-explorer.com/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/MQTT-Explorer-\([0-9].*\)\.exe/\1/p' | sed 's/-beta/./g'`
  # 0.4.0.1
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://mqtt-explorer.com/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*github\.com.*\/MQTT-Explorer-.*\.exe\)">portable.*/\1/p'`
  # https://github.com/thomasnordquist/MQTT-Explorer/releases/download/0.0.0-0.4.0-beta1/MQTT-Explorer-0.4.0-beta1.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*MQTT.*\/\(MQTT-Explorer-.*\.exe\)/\1/p'`
  # MQTT-Explorer-0.4.0-beta1.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  "${POAPPIN_PATH_TO_7ZIP}" x -aou -y \$PLUGINSDIR/app-64.7z
  rm -rf \$PLUGINSDIR
}

