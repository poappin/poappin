#!/bin/bash

POAPPIN_NAME="sublimetext"
POAPPIN_DESCRIPTION="Freeware disk space analyzer for Windows"
POAPPIN_HOMEPAGE="https://www.sublimetext.com/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.sublimetext.com/3"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*download\.sublimetext\.com\/Sublime Text Build .* x64\.zip\)">portable.*$/\1/p'`
  # https://download.sublimetext.com/Sublime Text Build 3211 x64.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*sublimetext.com\/\(Sublime.*x64\.zip\)$/\1/p'`
  # Sublime Text Build 3211 x64.zip

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/Sublime.*Build \([0-9].*[0-9]\) x64\.zip$/\1/p'`
  # 3211
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
}
