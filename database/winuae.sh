#!/bin/bash

POAPPIN_NAME="winuae"
POAPPIN_DESCRIPTION="WinUAE Amiga emulator"
POAPPIN_HOMEPAGE="https://www.winuae.net/"
POAPPIN_LICENSE="GNU GPL"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/WinUAE\([0-9].*\)_x64\.zip/\1/p'`
  # 4400
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.winuae.net/download/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*download\.abime\.net\/winuae.*WinUAE.*_x64\.zip\)">.*zip.*64-bit.*/\1/p'`
  # https://download.abime.net/winuae/releases/WinUAE4400_x64.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(WinUAE.*\.zip\)/\1/p'`
  # WinUAE4400_x64.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
