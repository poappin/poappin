#!/bin/bash

POAPPIN_NAME="calibre"
POAPPIN_DESCRIPTION="Powerful and easy to use e-book manager"
POAPPIN_HOMEPAGE="https://calibre-ebook.com/"
POAPPIN_LICENSE="GNU GPL v3"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/calibre-64bit-\([0-9].*\)\.msi/\1/p'`
  # 4.21.0
}

function poappin_app_check {
  # /download leads to /download_portable (portable exe) or /download_windows64 (MSI)
  #DATA=`${POAPPIN_GET_PAGE} "https://calibre-ebook.com/download_windows64"`

  # There is a portable version and also an msi installer.
  # Extract the portable version with: /d/calibre-portable-installer-4.20.0.exe d:\\calibre-portable
  # and the msi version with: msiexec /a d:\calibre-64bit-4.20.0.msi /qb TARGETDIR=d:\calibre-msi - or /qn
  # Comparisation: The portable setup contains additional files app\bin\API-MS-Win-core-xstate-l2-1-0.dll
  # and calibre-portable.exe. Not sure if they are really needed. ATM we use the msi archive.
  # wget -qO- https://www.fosshub.com/Calibre.html |  sed "s/\d032/\n/g;s/http/\nhttp/g;s/\.exe/\.exe\n/g" | grep -m1 "\.exe" 
  # https://www.fosshub.com/Calibre.html?dwl=calibre-portable-installer-4.21.0.exe

  # Link is: https://calibre-ebook.com/dist/win64 or alternative links "...msi"
  # The fosshub links are bad. Don't use them. We use the redirect from "/dist/win64".

  POAPPIN_URL=`${POAPPIN_GET_HEADERS} "https://calibre-ebook.com/dist/win64" 2>&1 | grep Location | sed -n 's/.*Location: \(https.*\)$/\1/p'`
  # https://download.calibre-ebook.com/4.21.0/calibre-64bit-4.21.0.msi

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(calibre-64bit-.*\.msi\)/\1/p'`
  # calibre-64bit-4.21.0.msi

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  rm calibre*.msi
  mv PFiles/Calibre2/* .
  rm -rf PFiles
}
