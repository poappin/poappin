#!/bin/bash

POAPPIN_NAME="uudeview"
POAPPIN_DESCRIPTION="Encoder and decoder for MIME Base64, yEnc, BinHex, uuencoding, and xxencoding methods"
POAPPIN_HOMEPAGE="http://www.miken.com/uud/"
POAPPIN_LICENSE="GNU GPL v2"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# Original homepage, not the Windows version:
# http://www.fpx.de/fp/Software/UUDeview/

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/UUD32_\([0-9]\)\(.*\)\.zip/\1.\2/p'`
  # 1.4
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "http://www.miken.com/uud/"`

  POAPPIN_URL="http://www.miken.com/uud/"`echo "${DATA}" | sed -n 's/.*<a href="\(uud.*\.zip\)">EXE only<\/a>.*/\1/p'`
  # http://www.miken.com/uud/uud32_14/UUD32_14.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(UUD.*\.zip\)/\1/p'`
  # UUD32_14.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
