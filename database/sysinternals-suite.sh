#!/bin/bash

POAPPIN_NAME="sysinternals-suite"
POAPPIN_DESCRIPTION="Microsoft's (Mark Russinovich) Sysinternals Troubleshooting Utilities"
POAPPIN_HOMEPAGE="https://docs.microsoft.com/de-de/sysinternals/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/SysinternalsSuite-\(20[0-9][0-9]-[0-9][0-9]-[0-9][0-9]\)\.zip/\1/p'`
  # 2020-09-18
}

function poappin_app_check {
  # or from:
  # https://docs.microsoft.com/de-de/sysinternals/downloads/
  # https://github.com/MicrosoftDocs/sysinternals/blob/live/sysinternals/downloads/index.md

  POAPPIN_URL="https://docs.microsoft.com/de-de/sysinternals/downloads/sysinternals-suite"

  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/^.*a href="\(https.*download\.sysinternals\.com.*Suite\.zip\)".*$/\1/p'`
  # https://download.sysinternals.com/files/SysinternalsSuite.zip

  POAPPIN_VERSION=`poappin_get_last_modified_date ${POAPPIN_URL}`
  # 2020-09-18

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/https.*files\/\(.*\)\.zip/\1/p'`"-${POAPPIN_VERSION}.zip"
  # SysinternalsSuite-2020-09-18.zip
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  reg add "HKEY_CURRENT_USER\Software\Sysinternals\Process Monitor" //f //v EulaAccepted //t REG_DWORD //d 1
}

function poappin_app_uninstall {
  reg delete "HKEY_CURRENT_USER\Software\Sysinternals" //f
}
