#!/bin/bash

POAPPIN_NAME="why-not-win11"
POAPPIN_DESCRIPTION="Helps identify why your PC is not Windows 11 Release Ready"
POAPPIN_HOMEPAGE="https://github.com/rcmaehl/WhyNotWin11"
POAPPIN_LICENSE="LGPL 3.0"
POAPPIN_LICENSE_URL="https://github.com/rcmaehl/WhyNotWin11/blob/main/LICENSE"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://github.com/rcmaehl/WhyNotWin11/releases"`

  POAPPIN_URL="https://github.com"`echo "${DATA}" | sed -n 's/.*<a href="\(\/rcmaehl\/WhyNotWin11\/.*\.exe\)" rel=.*/\1/p' | grep -v _x86 | head -n 1`
  # https://github.com/rcmaehl/WhyNotWin11/releases/download/2.4.1/WhyNotWin11.exe

  POAPPIN_VERSION=`echo "${POAPPIN_URL}" | sed -n 's/.*\/download\/\(.*\)\/WhyNotWin11\.exe/\1/p'`
  # 2.4.1

  POAPPIN_FILENAME="WhyNotWin11-${POAPPIN_VERSION}.exe"
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  cp "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" .
}
