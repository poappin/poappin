#!/bin/bash

POAPPIN_NAME="nuget"
POAPPIN_DESCRIPTION="Package manager for Microsoft development platforms"
POAPPIN_HOMEPAGE="https://www.nuget.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  # The whole web page has no parseable latest version number on it.
  # It comes all from the index.json file. We use both entries
  # (URL and version number) from the json file.

  DATA=`${POAPPIN_GET_PAGE} "https://dist.nuget.org/index.json"`
  #DATA=`${POAPPIN_GET_PAGE} "https://www.nuget.org/downloads"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*"\(https:.*win-x86-commandline\/latest.*nuget\.exe\)".*$/\1/p'`
  # https://dist.nuget.org/win-x86-commandline/latest/nuget.exe

  #  { "displayName": "nuget.exe - recommended latest", "version": "5.6.0", "url": "https://...", "releasedate": "2020-05-18T11:00:00.000Z" },
  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*nuget\.exe.*latest.*"version": "\([0-9].*\)", "url.*$/\1/p'`
  # 5.6.0

  # we add the version number to the downloaded file to avoid further downloads.
  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*latest\/\(nuget\)\.exe$/\1/p'`"-${POAPPIN_VERSION}.exe"
  # nuget-5.6.0.exe
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  cp "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_APPS_DIR}/${POAPPIN_NAME}-${POAPPIN_VERSION}/nuget.exe"
}
