#!/bin/bash

POAPPIN_NAME="birdfont"
POAPPIN_DESCRIPTION="Free font editor which lets you create vector graphics and export TTF, OTF and SVG fonts"
POAPPIN_HOMEPAGE="https://birdfont.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL="https://birdfont.org/license/EULA_Birdfont_free.html"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/birdfont-\([0-9].*\)-free\.exe/\1/p'`
  # 4.19.5
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://birdfont.org/purchase.php"`

  POAPPIN_URL=""`echo "${DATA}" | sed -n 's/.*<a href="\(https.*birdfont\.org\/download\/birdfont-.*-free\.exe\)">birdfont.*/\1/p' | head -n 1`
  # https://birdfont.org/download/birdfont-4.19.5-free.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*download\/\(birdfont-.*\.exe\)/\1/p'`
  # birdfont-4.19.5-free.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  rm -f uninstall.exe
}
