#!/bin/bash

POAPPIN_NAME="winmerge"
POAPPIN_DESCRIPTION="Tool for visual difference display and merging for files and directories"
POAPPIN_HOMEPAGE="https://winmerge.org/"
POAPPIN_LICENSE="GNU GPL v2"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://winmerge.org/downloads/"`

  # https://sourceforge.net/projects/winmerge/files/stable/2.16.6/WinMerge-2.16.6-Setup.exe/download?use_mirror=jztkft
  # <td class="left"><a href="https://downloads.sourceforge.net/winmerge/winmerge-2.16.6-x64-exe.zip" target="_blan

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*downloads\.sourceforge\.net\/winmerge\/winmerge.*x64.*\.zip\)" target=.*$/\1/p'`
  # https://downloads.sourceforge.net/winmerge/winmerge-2.16.6-x64-exe.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*winmerge\/\(winmerge-.*-x64-exe\.zip\)$/\1/p'`
  # winmerge-2.16.6-x64-exe.zip

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/winmerge-\([0-9].*[0-9]\)-x64-exe\.zip$/\1/p'`
  # 2.16.6
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"

  mv WinMerge/* .
  rm -rf WinMerge
}
