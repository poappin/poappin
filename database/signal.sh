#!/bin/bash

POAPPIN_NAME="signal"
POAPPIN_DESCRIPTION="Free and open-source cross-platform encrypted messaging service"
POAPPIN_HOMEPAGE="https://signal.org/en/"
POAPPIN_LICENSE="GNU GPL v3"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# To use the Signal desktop app, Signal must first be installed on your phone.

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/signal-desktop-win-\([0-9].*\)\.exe/\1/p'`
  # 1.36.3
}

function poappin_app_check {
  # https://signal.org/en/download/windows/" | gunzip - Javascript, data comes from a YAML file.
  DATA=`${POAPPIN_GET_PAGE} "https://updates.signal.org/desktop/latest.yml"`

  POAPPIN_URL="https://updates.signal.org/desktop/"`echo "${DATA}" | sed -n 's/.*url: \(signal-desktop-win.*\.exe\)/\1/p'`
  # https://updates.signal.org/desktop/signal-desktop-win-1.36.3.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*desktop\/\(signal-.*\.exe\)/\1/p'`
  # signal-desktop-win-1.36.3.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  rm "Uninstall Signal.exe"
  7z x \$PLUGINSDIR/app-64.7z
  rm -rf \$PLUGINSDIR
}
