#!/bin/bash

POAPPIN_NAME="vulkan-sdk"
POAPPIN_DESCRIPTION="Vulkan SDK"
POAPPIN_HOMEPAGE="https://vulkan.lunarg.com/sdk/home"
POAPPIN_LICENSE="misc"
POAPPIN_LICENSE_URL="https://vulkan.lunarg.com/license/#/license"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION="7zip"

# https://files.lunarg.com/ - debug version.
# https://vulkan.lunarg.com/content/view/latest-sdk-version-api

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/VulkanSDK-\([0-9].*\)-Installer\.exe/\1/p'`
  # 1.3.231.1
}

function poappin_app_check {
  POAPPIN_VERSION=`${POAPPIN_GET_PAGE} "https://vulkan.lunarg.com/sdk/latest/windows.txt"`
  # 1.3.231.1

  # 5 downloads per hour per IP unless we determine you are not a bot:
  # add ?Human=true to the URL to bypass this limit. Is this enough?

  POAPPIN_URL="https://sdk.lunarg.com/sdk/download/${POAPPIN_VERSION}/windows/vulkan_sdk.exe"

  POAPPIN_FILENAME="VulkanSDK-${POAPPIN_VERSION}-Installer.exe"
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  rm -rf \$PLUGINSDIR

  # TODO: Compare with an installation.

  # https://vulkan.lunarg.com/doc/sdk/1.2.141.2/windows/getting_started.html
}
