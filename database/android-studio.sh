#!/bin/bash

POAPPIN_NAME="android-studio"
POAPPIN_DESCRIPTION="Android Studio"
POAPPIN_HOMEPAGE="https://developer.android.com/studio"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/android-studio-ide-\([0-9].*[0-9]\)-windows\.zip$/\1/p'`
  # 193.6514223
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://developer.android.com/studio"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*href="\(https.*android-studio-ide.*windows\.zip\)".*/\1/p' | head -n 1`
  # https://redirector.gvt1.com/edgedl/android/studio/ide-zips/4.0.0.16/android-studio-ide-193.6514223-windows.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/https.*\/\(android-studio-ide.*windows\.zip\)$/\1/p'`
  # android-studio-ide-193.6514223-windows.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  mv android-studio/* .
  rm -rf android-studio
}
