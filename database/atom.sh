#!/bin/bash

POAPPIN_NAME="atom"
POAPPIN_DESCRIPTION="A hackable text editor for the 21st Century"
POAPPIN_HOMEPAGE="https://atom.io/"
POAPPIN_LICENSE="MIT license"
POAPPIN_LICENSE_URL="https://raw.githubusercontent.com/atom/atom/master/LICENSE.md"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/atom-x64-windows-\([0-9].*\)\.zip/\1/p'`
  # 1.48.0
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://github.com/atom/atom/releases/latest"`
  # redirects to: https://github.com/atom/atom/releases/tag/v1.48.0

  POAPPIN_URL="https://github.com"`echo "${DATA}" | sed -n 's/.*<a href="\(\/atom\/.*releases\/download\/.*atom-x64-windows\.zip\)" rel=.*$/\1/p'`
  # https://github.com/atom/atom/releases/download/v1.48.0/atom-x64-windows.zip

  POAPPIN_VERSION=`echo "${POAPPIN_URL}" | sed -n 's/https.*download\/v\([0-9].*[0-9]\)\/atom-x64-windows.*$/\1/p'`
  # 1.48.0

  POAPPIN_FILENAME="atom-x64-windows-${POAPPIN_VERSION}.zip"
  # atom-x64-windows-1.48.0.zip
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  mv Atom\ x64/* .
  sync
  rm -rf Atom\ x64
}
