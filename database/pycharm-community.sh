#!/bin/bash

POAPPIN_NAME="pycharm-community"
POAPPIN_DESCRIPTION="The Python IDE for Professional Developers"
POAPPIN_HOMEPAGE="https://www.jetbrains.com/de-de/pycharm/"
POAPPIN_LICENSE="Apache-2.0 BSD CDDL MIT-with-advertising"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/pycharm-community-\([0-9].*[0-9]\)\.exe$/\1/p'`
  # 2020.2
}

function poappin_app_check {
  # https://www.jetbrains.com/de-de/pycharm/download/#section=windows
  # PCC is the community, PCP professional version
  POAPPIN_URL="https://www.jetbrains.com/de-de/pycharm/download/download-thanks.html?platform=windows&code=PCC"

  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  POAPPIN_URL="https:"`echo "${DATA}" | sed -n 's/.* href="\(\/\/.*jetbrains\.com.*\)".*$/\1/p' | sed 's/PCP/PCC/g'`
  # https://data.services.jetbrains.com/products/download?code=PCC&platform=windows

  POAPPIN_URL=`${POAPPIN_GET_HEADERS} ${POAPPIN_URL} 2>&1 | grep Location | sed -n 's/.*Location: \(https.*\)$/\1/p' | head -n 1`
  # https://download.jetbrains.com/python/pycharm-community-2020.2.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*python\/\(pycharm-.*\.exe\)/\1/p'`
  # pycharm-community-2020.2.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"

  rm -rf \$PLUGINSDIR
}
