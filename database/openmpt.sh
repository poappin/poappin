#!/bin/bash

POAPPIN_NAME="openmpt"
POAPPIN_DESCRIPTION="Free tracker software for Windows"
POAPPIN_HOMEPAGE="https://openmpt.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://openmpt.org/download"`

  POAPPIN_URL="https://openmpt.org"`echo "${DATA}" | sed -n 's/.*a href="\(\/download_file\.php.*OpenMPT-.*-portable-x64\.zip\)" class=.*>*$/\1/p'`
  # https://openmpt.org/download_file.php?file=OpenMPT-1.29.01.00-portable-x64.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*file=\(.*\.zip\)/\1/p'`
  # OpenMPT-1.29.01.00-portable-x64.zip

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/OpenMPT-\([0-9].*[0-9]\)-portable.*\.zip$/\1/p'`
  # 1.29.01.00
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"

  mv OpenMPT-*/* .
  rm -rf OpenMPT-*
  sync
}
