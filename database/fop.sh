#!/bin/bash

POAPPIN_NAME="fop"
POAPPIN_DESCRIPTION="Apache Formatting Objects Processor"
POAPPIN_HOMEPAGE="https://xmlgraphics.apache.org/fop/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/fop-\([0-9].*\)-bin\.zip/\1/p'`
  # 2.5
}

function poappin_app_check {
  POAPPIN_URL="https://xmlgraphics.apache.org/fop/download.html"

  # <td><a href="http://www.apache.org/dyn/closer.cgi?filename=/xmlgraphics/fop/binaries/fop-2.5-bin.zip&amp;action=download">fop-2.5-bin.zip</a></td>
  POAPPIN_URL=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}" | sed -n 's/.*<a href="\(http.*apache.org.*\/fop\/binaries\/fop-[0-9].*-bin.zip.*\)">fop.*<\/a>.*$/\1/p' | sed 's/\&amp;/\&/g'`
  # http://www.apache.org/dyn/closer.cgi?filename=/xmlgraphics/fop/binaries/fop-2.5-bin.zip&action=download

  POAPPIN_VERSION=`echo "${POAPPIN_URL}" | sed -n 's/.*binaries\/fop-\([0-9].*[0-9]\)-bin.zip.*/\1/p'`
  # 2.5

  POAPPIN_FILENAME="${POAPPIN_NAME}-${POAPPIN_VERSION}-bin.zip"

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/fop-2.3-msys2.patch" https://issues.apache.org/jira/secure/attachment/12952166/fop-2.3-msys2.patch
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  mv ${POAPPIN_NAME}-*/* .
  rm -rf ${POAPPIN_NAME}-*

  # add patch: https://issues.apache.org/jira/browse/FOP-2834
  patch -p1 -i "${POAPPIN_DOWNLOADS_DIR}/fop-2.3-msys2.patch"
}
