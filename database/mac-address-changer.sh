#!/bin/bash

POAPPIN_NAME="mac-address-changer"
POAPPIN_DESCRIPTION="Changes Media Access Control (MAC) address on any network interface"
POAPPIN_HOMEPAGE="http://www.itsamples.com/mac-address-changer.html"
POAPPIN_LICENSE="No commercial use"
POAPPIN_LICENSE_URL="http://www.itsamples.com/licensing.html"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "http://www.itsamples.com/mac-address-changer.html"`

  POAPPIN_URL="http://www.itsamples.com"`echo "${DATA}" | sed -n 's/.*<a href=".\(\/downloads\/mac-address-changer-x64\.zip\)" title=.*>.*/\1/p'`
  # http://www.itsamples.com/downloads/mac-address-changer-x64.zip

  DATA=`echo "${DATA}" | sed -n 's/.*downloads.*zip.*Download.*64-bit.*revised \([A-Z].*\) \([0-9].*\), \(20[0-9][0-9]\)).*$/\1 \2 \3/p'`
  # April 17 2017

  POAPPIN_VERSION=`date +"%Y-%m-%d" -d "${DATA}"`
  # 2017-04-17

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*downloads\/\(mac-.*x64\)\.zip/\1/p'`"-${POAPPIN_VERSION}.zip"
  # mac-address-changer-x64-2017-04-17.zip
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
