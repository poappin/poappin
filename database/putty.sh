#!/bin/bash

POAPPIN_NAME="putty"
POAPPIN_DESCRIPTION="Free implementation of SSH and Telnet for Windows and Unix"
POAPPIN_HOMEPAGE="https://www.chiark.greenend.org.uk/~sgtatham/putty/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/putty-\([0-9].*\)\.zip/\1/p'`
  # 0.73
}

function poappin_app_check {
  POAPPIN_URL="https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html"

  POAPPIN_VERSION=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}" | sed -n "s/.*<h1.*>Download PuTTY: latest release (\([0-9].*\))<\/h1>/\1/p"`
  # 0.73

  POAPPIN_FILENAME="${POAPPIN_NAME}-${POAPPIN_VERSION}.zip"
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "https://the.earth.li/~sgtatham/putty/latest/w64/putty.zip"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
