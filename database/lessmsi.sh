#!/bin/bash

POAPPIN_NAME="lessmsi"
POAPPIN_DESCRIPTION="A tool to view and extract the contents of a Windows Installer (.msi) file"
POAPPIN_HOMEPAGE="https://lessmsi.activescott.com/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/lessmsi-v\([0-9].*[0-9]\)\.zip$/\1/p'`
  # 1.10.0
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} 'https://api.github.com/repos/activescott/lessmsi/releases/latest'`

  DATA=`echo "$DATA" | jq '. | del(.author) | { version: .name, tag: .tag_name, created: .created_at, asset: .assets[] | select(.content_type == "application/zip") } | del(.asset.uploader)'`

  POAPPIN_URL=`echo "$DATA" | jq '.asset.browser_download_url' | tr -d \"`
  # https://github.com/activescott/lessmsi/releases/download/v1.10.0/lessmsi-v1.10.0.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(lessmsi-v.*\.zip\)/\1/p'`
  # lessmsi-v1.10.0.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
