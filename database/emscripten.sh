#!/bin/bash

POAPPIN_NAME="emscripten"
POAPPIN_DESCRIPTION="LLVM-based toolchain for compiling to asm.js and WebAssembly"
POAPPIN_HOMEPAGE="https://emscripten.org/"
POAPPIN_LICENSE="MIT license and University of Illinois/NCSA Open Source License"
POAPPIN_LICENSE_URL="https://emscripten.org/docs/introducing_emscripten/emscripten_license.html"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION="git"
}

function poappin_app_check {
  # https://emscripten.org/docs/getting_started/downloads.html

  POAPPIN_VERSION="git"
  POAPPIN_FILENAME=""
  POAPPIN_URL=""
}

function poappin_app_fetch {
 :
}

function poappin_app_install {
  # or extract the downloaded zip from: https://github.com/emscripten-core/emsdk
  git clone https://github.com/emscripten-core/emsdk.git
  cd emsdk

  #git pull

  # Fetch the latest registry of available tools.
  #./emsdk update

  # Download and install the latest SDK tools.
  ./emsdk install latest

  # Make the "latest" SDK "active" for the current user. (writes .emscripten file)
  ./emsdk activate latest

  # Activate PATH and other environment variables in the current terminal
  source ./emsdk_env.sh
}
