#!/bin/bash

POAPPIN_NAME="vlc"
POAPPIN_DESCRIPTION="VLC media player"
POAPPIN_HOMEPAGE="https://www.videolan.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.videolan.org/vlc/download-windows.html"`

  # They offer only the 32 bit version as zip/7z archives on their homepage
  # but there is also a 64 bit version.
  POAPPIN_URL="https:"`echo "${DATA}" | sed -n 's/.*a href="\(\/\/get\.videolan\.org\/vlc.*win32\/.*\.7z\)".*$/\1/p' | sed 's/win32/win64/g'`
  # https://get.videolan.org/vlc/3.0.11/win64/vlc-3.0.11-win64.7z

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/https.*\/win64\/\(vlc-[0-9].*-win64\.7z\)/\1/p'`
  # vlc-3.0.11-win64.7z

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/vlc-\([0-9].*\)-win64\.7z/\1/p'`
  # 3.0.11
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  mv vlc-*/* .
  rm -rf "${POAPPIN_NAME}-${POAPPIN_VERSION}"
}
