#!/bin/bash

POAPPIN_NAME="olly-dbg"
POAPPIN_DESCRIPTION="OllyDbg is a 32-bit assembler level analysing debugger for Microsoft Windows"
POAPPIN_HOMEPAGE="http://www.ollydbg.de/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL="http://www.ollydbg.de/download.htm"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/olly-dbg-\([0-9].*\)\.zip/\1/p'`
  # 1.10
}

function poappin_app_check {
  # https://github.com/romanzaikin/OllyDbg-v1.10-With-Best-Plugins-And-Immunity-Debugger-theme-
  DATA=`${POAPPIN_GET_PAGE} "http://www.ollydbg.de/download.htm"`

  POAPPIN_URL="http://www.ollydbg.de/"`echo "${DATA}" | sed -n 's/.*<a href="\(odbg.*\.zip\)">Download OllyDbg.*final/\1/p'`
  # http://www.ollydbg.de/odbg110.zip

  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*<a href=".*\.zip">Download OllyDbg \([0-9].*\)<\/a>.*final/\1/p'`
  # 1.10

  POAPPIN_FILENAME="${POAPPIN_NAME}-${POAPPIN_VERSION}.zip"
  # olly-dbg-1.10.zip
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}

