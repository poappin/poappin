#!/bin/bash

POAPPIN_NAME="sqlitespy"
POAPPIN_DESCRIPTION="SQLiteSpy is a fast and compact GUI database manager for SQLite."
POAPPIN_HOMEPAGE="https://www.yunqa.de/delphi/products/sqlitespy/index"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# <a target="_blank" href="https://www.yunqa.de/delphi/downloads/SQLiteSpy_1.9.14.zip">

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/SQLiteSpy_\([0-9].*\)\.zip/\1/p'`
  # 1.9.14
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.yunqa.de/delphi/products/sqlitespy/index"`

  POAPPIN_URL=`echo "${DATA}" | sed -n "s/<a .* href='\(https.*delphi.*\/SQLiteSpy_.*\.zip\)'>.*/\1/p"`
  # https://www.yunqa.de/delphi/downloads/SQLiteSpy_1.9.14.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(SQLiteSpy_.*\.zip\)/\1/p'`
  # SQLiteSpy_1.9.14.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  mv win64/* .
  rm -rf win32 win64
}
