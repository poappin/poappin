#!/bin/bash

POAPPIN_NAME="mp3tag"
POAPPIN_DESCRIPTION="Powerful and easy-to-use tool to edit metadata of audio files"
POAPPIN_HOMEPAGE="https://www.mp3tag.de/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/mp3tagv\([0-9]\)\(.*\)setup\.exe/\1.\2/p'`
  # 3.04a
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.mp3tag.de/download.html"`

  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*<h2>Mp3tag v\([0-9].*[0-9].*\)<\/h2>$/\1/p'`
  # 3.04a

  DATA=`${POAPPIN_GET_PAGE} "https://www.mp3tag.de/dodownload.html"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*href="\(https.*download\.mp3tag\.de\/mp3tag.*\.exe\)"$/\1/p'`
  # https://download.mp3tag.de/mp3tagv304asetup.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/https.*mp3tag\.de\/\(mp3tag.*setup\.exe\)$/\1/p'`
  # mp3tagv304asetup.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  rm -rf \$PLUGINSDIR \$R0 \$R0_1 Mp3tagUninstall.exe.nsis
  touch mp3tag.cfg

  # the duplicate files are en and de files.
}
