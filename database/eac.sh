#!/bin/bash

POAPPIN_NAME="eac"
POAPPIN_DESCRIPTION="Exact Audio Copy is a so called audio grabber for audio CDs"
POAPPIN_HOMEPAGE="http://www.exactaudiocopy.de/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/eac-\([0-9].*\)\.exe/\1/p'`
  # 1.5
}

function poappin_app_check {
  # http://www.exactaudiocopy.de/en/index.php/resources/download/
  # Standard links are leading to fosshub.com, which would be a bad choice.
  DATA=`${POAPPIN_GET_PAGE} "http://www.exactaudiocopy.de/en/index.php/weitere-seiten/download-from-alternative-servers-2/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(http.*exactaudiocopy.de\/eac.*\.exe\)">Download.*$/\1/p'`
  # http://www.exactaudiocopy.de/eac-1.5.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\.de\/\(eac-.*\.exe\)/\1/p'`
  # eac-1.5.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  # extract with auto rename files. two manifests. second one includes a plugin handler.
  poappin_extract_file "${POAPPIN_FILENAME}"

  rm -rf \$PLUGINSDIR
  rm -f uninst.exe
}
