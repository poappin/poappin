#!/bin/bash

POAPPIN_NAME="teracopy"
POAPPIN_DESCRIPTION=""
POAPPIN_HOMEPAGE="http://www.codesector.com/teracopy"
POAPPIN_LICENSE="Freeware"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/TeraCopy-\([0-9].*\)\.exe/\1/p'`
  # 3.26
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.codesector.com/downloads"`

  POAPPIN_URL="https://www.codesector.com"`echo "${DATA}" | sed -n 's/.*<a href="\(\/files\/teracopy\.exe\)">.*$/\1/p'`
  # http://www.codesector.com/files/teracopy.exe

  # or: <tbody><tr><th>Version:</th><td><b>3.6</b></td></tr>
  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*<td>TeraCopy \([0-9].*[0-9]\)<\/td>$/\1/p' | head -n 2 | tail -n 1`
  # Select the 2nd of 3 version numbers. The 1st one is the mac version, 3rd one an old TeraCopy for Windows XP+.
  # 3.26

  POAPPIN_FILENAME="TeraCopy-${POAPPIN_VERSION}.exe"
  # TeraCopy-3.26.exe
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  #"${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  #TeraCopy-3.26.exe /verysilent /forcecloseapplications /suppressmsgboxes

  #poappin_extract_inno_setup_file "${POAPPIN_FILENAME}"

  ../innounp-0.49/innounp.exe -x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  mv \{app\}/* .
  rm -rf \{app\}

  # TODO: Registry, see install_script.iss
}
