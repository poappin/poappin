#!/bin/bash

POAPPIN_NAME="android-ndk"
POAPPIN_DESCRIPTION="Android Native Development Kit (NDK)"
POAPPIN_HOMEPAGE="https://developer.android.com/ndk/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/android-ndk-r\(.*\)-windows.*$/\1/p'`
  # 21d
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://developer.android.com/ndk/downloads"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*href="\(https.*android-ndk.*windows-x86_64\.zip\)".*/\1/p' | head -n 1`
  # https://dl.google.com/android/repository/android-ndk-r21d-windows-x86_64.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/https.*\/\(android-ndk.*\.zip\)$/\1/p'`
  # android-ndk-r21d-windows-x86_64.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  mv android-ndk-*/* .
  rm -rf android-ndk-*
}
