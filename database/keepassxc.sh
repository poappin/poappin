#!/bin/bash

POAPPIN_NAME="keepassxc"
POAPPIN_DESCRIPTION="KeePassXC is a community fork of KeePassX which aims to incorporate stalled pull requests, features, and bug fixes that have never made it into the main KeePassX repository"
POAPPIN_HOMEPAGE="https://keepassxc.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# https://www.keepassx.org/
# https://github.com/keepassxreboot/keepassx/issues/43

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/KeePassXC-\([0-9].*\)-Win64-Portable\.zip/\1/p'`
  # 2.6.1
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://keepassxc.org/download/#windows"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*github.*download.*KeePassXC-.*-Win64-Portable\.zip\)">/\1/p'`
  # https://github.com/keepassxreboot/keepassxc/releases/download/2.6.1/KeePassXC-2.6.1-Win64-Portable.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(KeePassXC-.*\.zip\)/\1/p'`
  # KeePassXC-2.6.1-Win64-Portable.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  mv KeePassXC-*-Win64/* .
  mv KeePassXC-*-Win64/.portable .
  rm -rf KeePassXC-*-Win64
}
