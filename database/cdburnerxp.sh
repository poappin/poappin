#!/bin/bash

POAPPIN_NAME="cdburnerxp"
POAPPIN_DESCRIPTION="CDBurnerXP is a free application to burn CDs and DVDs, including Blu-Ray and HD-DVDs"
POAPPIN_HOMEPAGE="https://cdburnerxp.se/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/CDBurnerXP-x64-\([0-9].*\)\.zip/\1/p'`
  # 4.5.8.7128
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://cdburnerxp.se/de/download"`

  POAPPIN_URL=""`echo "${DATA}" | sed -n 's/.*<a href="\(https.*portable.*CDBurnerXP-x64-.*\.zip\)" .*/\1/p'`
  # https://download.cdburnerxp.se/portable/CDBurnerXP-x64-4.5.8.7128.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(CDBurnerXP-x64-.*\.zip\)/\1/p'`
  # CDBurnerXP-x64-4.5.8.7128.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
