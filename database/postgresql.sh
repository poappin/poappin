#!/bin/bash

POAPPIN_NAME="postgresql"
POAPPIN_DESCRIPTION="PostgreSQL is a powerful, open source object-relational database system."
POAPPIN_HOMEPAGE="https://www.postgresql.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL="https://www.postgresql.org/about/licence/"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/postgresql-\([0-9].*\)-windows-x64-.*\.zip/\1/p' | sed 's/-/./g'`
  # 13.1.1
}

function poappin_app_check {
  # https://www.postgresql.org/download/windows/
  DATA=`${POAPPIN_GET_PAGE} "https://www.enterprisedb.com/download-postgresql-binaries"`

  POAPPIN_URL=""`echo "${DATA}" | sed -n 's/.*<a class="windows" href="\(https.*getfile.*fileid.*\)"><img.*windows.*x86-64.*/\1/p' | head -n 1`
  # first link should be windows and latest version
  # https://sbp.enterprisedb.com/getfile.jsp?fileid=1257397

  POAPPIN_URL=`${POAPPIN_GET_HEADERS} ${POAPPIN_URL} 2>&1 | grep Location | sed -n 's/.*Location: \(http.*\)$/\1/p'`
  # https://get.enterprisedb.com/postgresql/postgresql-13.1-1-windows-x64-binaries.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*postgresql\/\(postgresql-.*\.zip\)/\1/p'`
  # postgresql-13.1-1-windows-x64-binaries.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  mv pgsql/* .
  rm -rf pgsql
}
