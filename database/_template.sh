#!/bin/bash

POAPPIN_NAME=""
POAPPIN_DESCRIPTION=""
POAPPIN_HOMEPAGE=""
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/xxx-\([0-9].*\)\.msi/\1/p'`
  # 
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "insert URL to download page here"`

  POAPPIN_URL=""`echo "${DATA}" | sed -n 's/.*a href="\(\/xxx\)">/\1/p'`
  # 

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(xxx-.*\.msi\)/\1/p'`
  # 

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
