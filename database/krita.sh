#!/bin/bash

POAPPIN_NAME="krita"
POAPPIN_DESCRIPTION="Professional FREE and open source painting program"
POAPPIN_HOMEPAGE="https://krita.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://krita.org/en/download/krita-desktop/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*href=\(https:.*krita.*x64.*\.zip\) >/\1/p'`
  # https://download.kde.org/stable/krita/4.3.0/krita-x64-4.3.0.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(krita-x64.*\.zip\)$/\1/p'`
  # krita-x64-4.3.0.zip

  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/krita-x64-\([0-9].*\)\.zip/\1/p'`
  # 4.3.0
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  mv krita-x64-*/* .
  rm -rf krita-x64-*
}
