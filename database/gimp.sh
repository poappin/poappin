#!/bin/bash

POAPPIN_NAME="gimp"
POAPPIN_DESCRIPTION="GNU Image Manipulation Program (GIMP)"
POAPPIN_HOMEPAGE="https://www.gimp.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION="innounp"

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/gimp-\([0-9].*\)-setup-1\.exe/\1/p'`
  # 2.10.20
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.gimp.org/downloads/"`

  POAPPIN_URL="https:"`echo "${DATA}" | sed -n 's/.*href="\(\/\/download\.gimp\.org.*-setup.*\.exe\)"/\1/p'`
  # https://download.gimp.org/mirror/pub/gimp/v2.10/windows/gimp-2.10.20-setup-1.exe

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*windows\/\(gimp-.*\.exe\)/\1/p'`
  # gimp-2.10.20-setup-1.exe

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  #poappin_extract_inno_setup_file "${POAPPIN_FILENAME}"

  ../innounp-0.49/innounp.exe -x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"

  find . -name "*.debug" -type d -exec rm -rf {} \;

  mv \{app\}/* .
  rm -rf \{app\} \{tmp\}

  # [Dirs]
  mkdir -p share/gimp/2.0/fonts

  # [Registry]
  # cat apps/gimp-2.10.20/install_script.iss | grep -ve "^; " | grep -v "^Source: " | grep -v "{sys}" | more

  # Files: all "Source:" lines without sys (dll/files check) and tmp (delete after install):
  cat install_script.iss | grep -ve "^; " | grep "^Source: " | grep -v "{sys}" | grep -v "{tmp}" > sources.poappin

  # Show the different component markers:
  # cat sources.poappin | sed -n 's/.*Components:\(.*\); MinVersion.*$/\1/p' | sort | uniq

  # Mark files for removing or installing.
  # kein deps32, da sonst Überschreibungen mit deps64; gimp32on64 nein, zwar in extra Verzeichnis und stört nicht,
  # aber Verzeichnis wird nicht gefunden beim "mv". (not gimp32on64) and gimp64 ja, da nur twain.exe, falls nicht
  # bereits vorhanden und gimp32on64 ist ja nein. deps32\compat sind nur
  # eine handvoll Dateien, keine Konflikte mit 64-Bit (keine Überschreibungen, zusätzlich), aber auch "32" Verzeichnis.
  cat sources.poappin | sed -re 's/Components: (deps|gimp)(32|64) and debug;/DONTINSTALL/g' | sed -re 's/Components: gs and (deps|gimp)(32|64);/DONTINSTALL/g' | sed -re 's/Components: gs and .gimp32 or gimp64.;/DONTINSTALL/g' | sed -re 's/Components: py and gimp32;/DONTINSTALL/g' | sed -re 's/Components: gimp32;/DONTINSTALL/g' | sed 's/Components: loc;/INSTALLME/g' | sed 's/Components: mypaint;/INSTALLME/g' | sed 's/Components: gimp64;/INSTALLME/g' | sed 's/Components: deps64;/INSTALLME/g' | sed 's/Components: gimp32 or gimp64;/INSTALLME/g' | sed 's/Components: deps32 or deps64;/INSTALLME/g' | sed 's/Components: py;/INSTALLME/g' | sed 's/Components: py and gimp64;/INSTALLME/g' | sed 's/Components: deps32\\wimp or deps64\\wimp;/INSTALLME/g' | sed 's/Components: gimp32on64 and deps64\\wimp;/DONTINSTALL/g' | sed 's/Components: gimp32on64\\compat;/DONTINSTALL/g' | sed 's/Components: gimp32on64;/DONTINSTALL/g' | sed 's/Components: (not gimp32on64) and gimp64;/INSTALLME/g' | sed 's/Components: deps32;/DONTINSTALL/g' | sed 's/Components: deps32\\compat;/INSTALLME/g' > sources.poappin.marked

  cat sources.poappin.marked | grep DONTINSTALL | sed -n 's/Source: "{app}\\\(.*\)"; DestDir: .*$/\1/p;' | sed 's/\\/\\\\/g' | xargs --max-lines=5 rm -f

  cat sources.poappin.marked | grep INSTALLME | sed -n 's/Source: "{app}\\\(.*\)"; DestDir: "{app}\\\(.*\)"; DestName: "\(.*\)"; INSTALLME.*/\1 \2\\\3/p' | sed 's/\\/\\\\/g' | xargs --max-lines=1 mv
  cat sources.poappin.marked | grep INSTALLME | sed "s/Source: .* DestDir: .* DestName: .* INSTALLME .*/HHHH/g" | grep -v HHHH > sources.poappin.marked2

  #mv: der Aufruf von stat für 'bin\libpython2.7,2.dll' ist nicht möglich: No such file or directory
  #mv: das angegebene Ziel '(dev).tmpl' ist kein Verzeichnis
  #mv: das angegebene Ziel 'manifest.xml' ist kein Verzeichnis

  cat sources.poappin.marked2 | grep INSTALLME | sed -n 's/Source: "{app}\\\(.*\)"; DestDir: "{app}\\\(.*\)"; INSTALLME.*/\1 \2\\/p' | sed 's/\\/\\\\/g' | xargs --max-lines=1 mv
  cat sources.poappin.marked2 | grep INSTALLME | sed "s/Source: .* DestDir: .* INSTALLME .*/HHHH/g" | grep -v HHHH
  # Finished, there are no unhandled files.
}
