#!/bin/bash

POAPPIN_NAME="obs-studio"
POAPPIN_DESCRIPTION="Open Broadcaster Software Studio - Free and open source software for live streaming and screen recording"
POAPPIN_HOMEPAGE="https://obsproject.com/"
POAPPIN_LICENSE="GNU GPL 2.0+"
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# https://github.com/obsproject/obs-studio

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/OBS-Studio-\([0-9].*\)-Full-x64\.zip/\1/p'`
  # 27.1.1
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://obsproject.com/download"`

  POAPPIN_URL=""`echo "${DATA}" | sed -n 's/.*<a href="\(https:\/\/.*x64\.zip\)".*/\1/p'`
  # https://cdn-fastly.obsproject.com/downloads/OBS-Studio-27.1.1-Full-x64.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(OBS-Studio-.*\.zip\)/\1/p'`
  # OBS-Studio-27.1.1-Full-x64.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
