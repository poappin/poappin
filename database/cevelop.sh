#!/bin/bash

POAPPIN_NAME="cevelop"
POAPPIN_DESCRIPTION="Eclipse-based C++ IDE for professional developers."
POAPPIN_HOMEPAGE="https://cevelop.com/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL="https://cevelop.com/license/"
POAPPIN_REQUIRED_TO_RUN="eclipse-adoptium-jre"
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# Needs Java 8.

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/cevelop-\([0-9].*\)-2020.*\.zip/\1/p'`
  # 1.14.1
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://cevelop.com/download/"`

  POAPPIN_URL="https://cevelop.com"`echo "${DATA}" | sed -n 's/.*<a class=.* href="\(\/cevelop\/downloads\/.*win32\.x86_64\.zip\)" role=.*>.*/\1/p'`
  # https://cevelop.com/cevelop/downloads/cevelop-1.14.1-202002280945-win32.win32.x86_64.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(cevelop-.*\.zip\)/\1/p'`
  # cevelop-1.14.1-202002280945-win32.win32.x86_64.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  mv cevelop/.eclipseproduct cevelop/* .
  rm -rf cevelop

  rm -f run.bat
  local JREPATH=`ls -1 .. | grep eclipse-adoptium-jre | head -n 1`
  echo "@echo off" >> run.bat
  echo "SET JAVA_HOME=%~dp0\\..\\${JREPATH}" >> run.bat
  echo "SET PATH=%PATH%;%JAVA_HOME%\\bin" >> run.bat
  echo "cevelop.exe" >> run.bat
}
