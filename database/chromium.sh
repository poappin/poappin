#!/bin/bash

POAPPIN_NAME="chromium"
POAPPIN_DESCRIPTION="Open-source web browser"
POAPPIN_HOMEPAGE="https://www.chromium.org/Home"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# https://commondatastorage.googleapis.com/chromium-browser-snapshots/index.html?prefix=Win_x64/
# A file "LAST_CHANGE" contains the latest revision number.

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/chromium-win64-revision-\([0-9].*\)\.zip/\1/p'`
  # 786694
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://download-chromium.appspot.com/?platform=Win_x64&type=snapshots"`

  POAPPIN_URL="https://download-chromium.appspot.com"`echo "${DATA}" | sed -n 's/.*a.*download.* href="\(\/dl\/Win_x64.type=snapshots\)">/\1/p'`
  # https://download-chromium.appspot.com/dl/Win_x64?type=snapshots
  # redirects to:
  # https://commondatastorage.googleapis.com/chromium-browser-snapshots/Win_x64/786689/chrome-win.zip

  # Get revision number of the build: {"content": "786689", "last-modified": "Thu, 09 Jul 2020 11:34:01 GMT"}
  POAPPIN_VERSION=`${POAPPIN_GET_PAGE} "https://download-chromium.appspot.com/rev/Win_x64?type=snapshots" | sed 's/.*content": "\([0-9]*\)"\, "last-modified".*/\1/'`
  # 786694

  POAPPIN_FILENAME="chromium-win64-revision-${POAPPIN_VERSION}.zip"
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  mv chrome-win/* .
  rm -rf chrome-win
}
