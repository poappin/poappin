#!/bin/bash

POAPPIN_NAME="mpg123"
POAPPIN_DESCRIPTION="Fast console MPEG Audio Player and decoder library"
POAPPIN_HOMEPAGE="https://www.mpg123.de/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# https://sourceforge.net/projects/mpg123/files/
# https://www.mpg123.de/cgi-bin/scm/mpg123/

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/mpg123-\([0-9].*\)-x86-64\.zip/\1/p'`
  # 1.26.4
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.mpg123.de/download.shtml"`

  POAPPIN_URL="https://www.mpg123.de/"`echo "${DATA}" | sed -n 's/.* <a href="\(download\/win64\/.*\/\)">download.*/\1/p'`
  # https://www.mpg123.de/download/win64/1.26.4/

  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  POAPPIN_URL="${POAPPIN_URL}"`echo "${DATA}" | sed -n 's/.*<a href="\(mpg123-.*-x86-64\.zip\)">mpg123.*/\1/p' | grep -v static`
  # mpg123-1.26.4-x86-64.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(mpg123-.*-x86-64\.zip\)/\1/p'`
  # mpg123-1.26.4-x86-64.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  mv mpg123-*/* .
  rm -rf mpg123-*-64
}

