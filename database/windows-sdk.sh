#!/bin/bash

POAPPIN_NAME="windows-sdk"
POAPPIN_DESCRIPTION="The Windows SDK (10.0.22621) for Windows 11, version 22H2 provides the latest headers, libraries, metadata, and tools for building Windows applications. Use this SDK to build Universal Windows Platform (UWP) and Win32 applications for Windows 11, version 22H2 and previous Windows releases."
POAPPIN_HOMEPAGE="https://developer.microsoft.com/de-de/windows/downloads/windows-sdk/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# Extracts only the setup files. No real installation ATM.

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/windows-sdk-\([0-9].*\)\.iso/\1/p'`
  # 10.0.19041.0
}

#<p>Last updated: October 4, 2021</p>

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://developer.microsoft.com/en-us/windows/downloads/windows-sdk/"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https:\/\/go\..*linkid=.*\)" data-linktype=.*>Download the .iso .*/\1/p'`
  # https://go.microsoft.com/fwlink/p/?linkid=2196240

  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*The Windows SDK (\([0-9].*\)) for Windows 11.*$/\1/p'`
  # 10.0.22621

  POAPPIN_FILENAME="${POAPPIN_NAME}-${POAPPIN_VERSION}.iso"
  # windows-sdk-10.0.19041.0.iso
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
