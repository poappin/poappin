#!/bin/bash

POAPPIN_NAME="wipefile"
POAPPIN_DESCRIPTION="Deletes files and folders secure and fast"
POAPPIN_HOMEPAGE="https://www.gaijin.at/de/"
POAPPIN_LICENSE="Freeware"
POAPPIN_LICENSE_URL="https://www.gaijin.at/en/license-agreement-freeware"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
  # need Accept header and User-Agent - else we get Error 403: Forbidden.
  DATA=`${POAPPIN_GET_PAGE} --header="Accept: text/html" --user-agent="Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:40.0) Gecko/20100101 Firefox/40.0" "https://www.gaijin.at/en/software/wipefile"`

  # https://www.gaijin.at/en/software/wipefile?action=download
  # https://www.gaijin.at/getit?id=wipefile&vk=4hmqiR_4rWLJ0
  # https://www.gaijin.at/getit?id=wipefile&vk=4hmqCm_u7sBhG&ld=1
  # wipefile.7z

  # <div class="download_button"><a href="/getit?id=wipefile&amp;vk=4hmqiR_4rWLJ0">DOWNLOAD</a></div>
  # <p class="small" style="padding-left:32px;"><a href="?action=download">Linkable Download</a></p>
  POAPPIN_URL="https://www.gaijin.at"`echo "${DATA}" | sed -n 's/.*download_button.*<a href="\(\/getit.*wipefile.*\)">.*$/\1/p' | sed 's/\&amp;/\&/g'`
  # https://www.gaijin.at/getit?id=wipefile&vk=4hm6VG_nRcxXW

  # <tbody><tr><th>Version:</th><td><b>3.6</b></td></tr>
  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*Version:.*<b>\([0-9].*[0-9]\)<\/b>.*$/\1/p'`
  # 3.6

  POAPPIN_FILENAME="wipefile-${POAPPIN_VERSION}.7z"
  # wipefile-3.6.7z
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} --header="Accept: text/html" --user-agent="Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:40.0) Gecko/20100101 Firefox/40.0" -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
}
