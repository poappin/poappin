#!/bin/bash

POAPPIN_NAME="freac"
POAPPIN_DESCRIPTION="Free audio converter and CD ripper"
POAPPIN_HOMEPAGE="https://freac.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/freac-\([0-9].*\)-windows-x64\.zip/\1/p'`
  # 1.1.2b
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://freac.org/downloads-mainmenu-33"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*<a href="\(https.*releases\/download.*\/freac-[0-9].*-windows-x64\.zip\)">.*/\1/p'`
  # https://github.com/enzo1982/freac/releases/download/v1.1.2/freac-1.1.2b-windows-x64.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(freac-.*\.zip\)/\1/p'`
  # freac-1.1.2b-windows-x64.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
  mv freac-1.1.2b-x64/* .
  rm -rf freac-*
}

