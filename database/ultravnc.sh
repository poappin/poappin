#!/bin/bash

POAPPIN_NAME="ultravnc"
POAPPIN_DESCRIPTION="Powerful, easy to use and free remote pc access software"
POAPPIN_HOMEPAGE="https://www.uvnc.com/"
POAPPIN_LICENSE="GNU GPL v2"
POAPPIN_LICENSE_URL="https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/UltraVNC-\([0-9].*[0-9]\)\.zip/\1/p'`
  # 1.2.40
}

# https://www.uvnc.eu/download/1250/UltraVNC_1_2_50_X64_Setup.exe
# https://www.uvnc.eu/download/1250/UltraVNC_1_2_5_new_bins.zip

# UltraVNC_1_2_50_X64_Setup.exe /VERYSILENT /NORESTART /RESTARTEXITCODE=3010 /SP- /SUPPRESSMSGBOXES /CLOSEAPPLICATIONS /FORCECLOSEAPPLICATIONS /NOICONS /DIR="KKKKK" [/SILENT}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.uvnc.com/downloads/ultravnc.html"`

  POAPPIN_VERSION=`echo "${DATA}" | sed -n 's/.*>Latest version \([0-9].*[0-9]\)<br\>.*/\1/p'`
  # 1.2.4.0

  POAPPIN_URL="https://www.uvnc.com"`echo "${DATA}" | sed -n 's/.*<a href="\(\/downloads\/ultravnc\/.*\.html\)">$/\1/p' | grep -v release-candidate | head -n 1`
  # https://www.uvnc.com/downloads/ultravnc/129-download-ultravnc-1231.html

  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  #echo "All links:"
  #echo "${DATA}" | sed -n 's/.*<a.*href="\(\/.*\.html\)" alt="Download".*$/\1/p'

  POAPPIN_URL="https://www.uvnc.com"`echo "${DATA}" | sed -n 's/.*<a.*href="\(\/component.*ultravnc.*bin-zip-all.*\.html\)" alt="Download".*$/\1/p'`
  # https://www.uvnc.com/component/jdownloads/summary/396-ultravnc-1240-bin-zip-all.html

  DATA2=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`
  local FORM_URL="https://www.uvnc.com"`echo "${DATA2}" | sed -n 's/.*<form action="\(\/component\/jdownloads\/send.*Itemid=0\)" method="post".*$/\1/p'`
  # https://www.uvnc.com/component/jdownloads/send/0-/396-ultravnc-1240-bin-zip-all.html?Itemid=0

  local FORM_HIDDEN_KEY=`echo "${DATA2}" | sed -n 's/.*<input type="hidden" name="\(.*\)" value="1" \/>.*$/\1/p'`
  # 4513e3de552c7c89bdfcd9747b8cf4e5

  local FORM_DATA=`wget -qO- --user-agent="Mozilla/5.0" --keep-session-cookies --save-cookies cookies.txt --post-data="license_agree=true&submit=Download&${FORM_HIDDEN_KEY}=1" "${FORM_URL}"`
  # <script>document.location.href='https://www.uvnc.eu/download/1240/UltraVNC_1_2_40.zip';</script>

  POAPPIN_URL=`echo "${FORM_DATA}" | sed -n 's/.*\.href=.\(https.*uvnc.*download.*UltraVNC.*\.zip\).;.*$/\1/p'`
  # https://www.uvnc.eu/download/1240/UltraVNC_1_2_40.zip

  # --referer=https://www.uvnc.com/ --mirror

  # needed?
  # /component/jdownloads/summary/294-addons-x64-1210.html
  # /component/jdownloads/summary/327-ultravnc-1212-dsm-bin-zip.html

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(UltraVNC.*\.zip\)$/\1/p' | sed 's/UltraVNC_/UltraVNC-/g' | sed 's/_/./g'`
  # UltraVNC-1.2.40.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
