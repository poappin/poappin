#!/bin/bash

POAPPIN_NAME="kotlin-compiler"
POAPPIN_DESCRIPTION="A modern programming language."
POAPPIN_HOMEPAGE="https://kotlinlang.org/"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/kotlin-compiler-\([0-9].*\)\.zip/\1/p'`
  # 1.4.21
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://github.com/JetBrains/kotlin/releases/latest"`

  POAPPIN_URL="https://github.com"`echo "${DATA}" | sed -n 's/.*<a href="\(\/JetBrains\/kotlin.*\/kotlin-compiler-.*\.zip\)" rel=.*>/\1/p'`
  # https://github.com/JetBrains/kotlin/releases/download/v1.4.21/kotlin-compiler-1.4.21.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(kotlin-compiler-.*\.zip\)/\1/p'`
  # kotlin-compiler-1.4.21.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
  mv kotlinc/* .
  rm -rf kotlinc
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"
}
