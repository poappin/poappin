#!/bin/bash

POAPPIN_NAME="cpu-z"
POAPPIN_DESCRIPTION="Gathers information on some of the main devices of your system"
POAPPIN_HOMEPAGE="https://www.cpuid.com/softwares/cpu-z.html"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

# Needs admin rights for installing a driver, but works without.

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/cpu-z_\([0-9].*[0-9]\)-en\.zip$/\1/p'`
  # 1.92
}

function poappin_app_check {
  DATA=`${POAPPIN_GET_PAGE} "https://www.cpuid.com/softwares/cpu-z.html"`

  POAPPIN_URL="https://www.cpuid.com"`echo "${DATA}" | sed -n 's/.*a class="button icon-zip" href="\(\/downloads\/cpu-z\/cpu-z.*\.zip\)">.*zip.*english.*64-bit.*$/\1/p' | head -n 1`
  # https://www.cpuid.com/downloads/cpu-z/cpu-z_1.92-en.zip

  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*a class="button" href="\(http.*cpuid\.com\/cpu-z\/cpu-z.*\.zip\)">.*DOWNLOAD NOW.*$/\1/p'`
  # http://download.cpuid.com/cpu-z/cpu-z_1.92-en.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/.*\/\(cpu-z_.*\.zip\)/\1/p'`
  # cpu-z_1.92-en.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  poappin_extract_file "${POAPPIN_FILENAME}"

  iconv -f utf-16le -t utf-8 cpuz.ini > cpuz-utf8.ini
  sed -i 's/^CheckUpdates=.*$/CheckUpdates=0/g' cpuz-utf8.ini
  #iconv -f utf-8 -t utf-16 cpuz-utf8.ini > cpuz-utf16.ini
  iconv -f utf-8 -t utf-16le cpuz-utf8.ini > cpuz-utf16le.ini
  mv -f cpuz-utf16le.ini cpuz.ini

  # https://stackoverflow.com/questions/8923866/convert-utf8-to-utf16-using-iconv/8924403#8924403
  # 
  # manual addition of the BOM
  # run od -c on the files to see their actual contents
  # ( printf "\xff\xfe" ; iconv -f utf-8 -t utf-16le UTF-8-FILE ) > UTF-16-FILE
  # iconv -f utf-8 -t utf-16 UTF-8-FILE | dd conv=swab 2>/dev/null
  #
  # first convert to UTF-16, which will prepend a byte-order mark, if necessary.
  # Then since UTF-16 doesn't define endianness, we must use file to determine
  # whether it's UTF-16BE or UTF-16LE. Finally, we can convert to UTF-16LE.
  # iconv -f utf-8 -t utf-16 UTF-8-FILE > UTF-16-UNKNOWN-ENDIANNESS-FILE
  # FILE_ENCODING="$( file --brief --mime-encoding UTF-16-UNKNOWN-ENDIANNESS-FILE )"
  # iconv -f "$FILE_ENCODING" -t UTF-16LE UTF-16-UNKNOWN-ENDIANNESS-FILE > UTF-16-FILE 
}
