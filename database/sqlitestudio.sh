#!/bin/bash

POAPPIN_NAME="sqlitestudio"
POAPPIN_DESCRIPTION="Create, edit, browse SQLite databases"
POAPPIN_HOMEPAGE="https://sqlitestudio.pl/"
POAPPIN_LICENSE="GNU GPL v3+ with OpenSSL exception"
POAPPIN_LICENSE_URL="https://github.com/pawelsalawa/sqlitestudio/blob/master/LICENSE"
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_check {
#  DATA=`${POAPPIN_GET_PAGE} "https://github.com/pawelsalawa/sqlitestudio/releases"`
#
#  POAPPIN_URL="https://github.com"`echo "${DATA}" | sed -n 's/.*href="\(\/pawelsalawa\/sqlitestudio.*SQLiteStudio-.*\.zip\)" rel=.*$/\1/p'`
#  # https://github.com/pawelsalawa/sqlitestudio/releases/download/3.2.1/SQLiteStudio-3.2.1.zip
#
#  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/https.*releases.*\/\(SQLiteStudio-.*\.zip\)$/\1/p'`
#  # SQLiteStudio-3.2.1.zip
#
#  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/SQLiteStudio-\([0-9].*\)\.zip/\1/p' | sed 's/\([0-9]\)\([0-9]\)\([0-9]\{2\}\)/\1.\2.\3/'`
#  # 3.2.1

  DATA=`${POAPPIN_GET_PAGE} 'https://api.github.com/repos/pawelsalawa/sqlitestudio/releases/latest'`
  DATA=`echo "$DATA" | jq '. | del(.author) | { version: .name, tag: .tag_name, asset: .assets[] | select(.content_type == "application/x-zip-compressed") } | del(.asset.uploader) | select(.asset.name | startswith("sqlitestudio"))'`

  POAPPIN_URL=`echo "$DATA" | jq '.asset.browser_download_url' | tr -d \"`
  POAPPIN_VERSION=`echo "$DATA" | jq '.version' | tr -d \" | sed -n 's/^\([0-9].*[0-9]\) .*$/\1/p'`
  POAPPIN_FILENAME=`basename ${POAPPIN_URL}`
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
  mv SQLiteStudio/* .
  rm -rf SQLiteStudio
}
