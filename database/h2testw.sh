#!/bin/bash

POAPPIN_NAME="h2testw"
POAPPIN_DESCRIPTION="heise.de memory card and usb stick testing application"
POAPPIN_HOMEPAGE="https://www.heise.de/ct/artikel/c-t-Systeminfo-2859100.html"
POAPPIN_LICENSE=""
POAPPIN_LICENSE_URL=""
POAPPIN_REQUIRED_TO_RUN=""
POAPPIN_REQUIRED_FOR_INSTALLATION=""

function poappin_app_set_version {
  POAPPIN_VERSION=`echo "${POAPPIN_FILENAME}" | sed -n 's/.*_\([0-9].*[0-9]\)\.zip/\1/p'`
  # 1.4
}

function poappin_app_check {
  POAPPIN_URL="https://www.heise.de/ct/artikel/c-t-Systeminfo-2859100.html"
  DATA=`${POAPPIN_GET_PAGE} "${POAPPIN_URL}"`

  # <a href="ftp://ftp.heise.de/pub/ct/ctsi/h2testw_1.4.zip">h2testw_1.4.zip </a>
  POAPPIN_URL=`echo "${DATA}" | sed -n 's/.*a href="\(ftp:.*h2testw.*\.zip\)">h2testw.*$/\1/p'`
  # ftp://ftp.heise.de/pub/ct/ctsi/h2testw_1.4.zip

  POAPPIN_FILENAME=`echo "${POAPPIN_URL}" | sed -n 's/^ftp.*\/\(h2testw_.*\.zip\)$/\1/p'`
  # h2testw_1.4.zip

  poappin_app_set_version
}

function poappin_app_fetch {
  ${POAPPIN_GET_FILE} -O "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" "${POAPPIN_URL}"
}

function poappin_app_install {
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}"
}
