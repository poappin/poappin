#!/bin/bash

# This file contains the portable applications installer. Its functions
# allow to update, check, download and install packages into a portable
# directory (e.g. a shared directory on a Windows server).
#
# Run a MSYS shell and source this file!

# TODO:
#
# * Add GUI tool and don't use bash scripts anymore.
# * Hard links.
# * Backup and restore apps.
# * Allow apps with own binary sources.
# * Check SHA-checksums if provided by the publisher.
# * Check if application-version already exists while
#   searching for updates.
# * Handle user variables.

# Retrieves the local root directory. This only works
# while sourcing the poappin.sh. So call it only at this time.
# POAPPIN_BASE_DIR is now e.g. "/p/projekte/poappin"

function poappin_find_base_dir {
  if [[ $_ != $0 ]]; then                                            # Script is being sourced.
    local ABSPATHNAME=""

    if [ -f "${BASH_SOURCE[0]}" ]; then                              # Get first element of ${BASH_SOURCE[@]} array.
      pushd . >/dev/null
      cd `dirname "${BASH_SOURCE[0]}"`
      ABSPATHNAME=`pwd`"/"`basename "${BASH_SOURCE[0]}"`
      popd >/dev/null
    else
      echo "Error retrieving absolute path name to poappin.sh."
      return
    fi

    if [ "$POAPPIN_DEBUG" = "1" ]; then
      echo "ABSPATHNAME of poappin.sh: $ABSPATHNAME"
    fi

    if [[ ! "${ABSPATHNAME:0:1}" == '/' ]]; then
      echo "Absolute path name to poappin.sh is not absolute: $ABSPATHNAME"
      return
    fi

    export POAPPIN_BASE_DIR=`echo "${ABSPATHNAME}" | sed 's/\/poappin\.sh//'`
  else
    echo "Error: The script poappin.sh can't get executed, please source it instead."
    return
  fi
}

# This function "sources" the database script for a package,
# which defines some functions and variables.

function poappin_load_package {
  if [ ! $# -gt 0 ]; then
    echo "Usage: $FUNCNAME <package name>"
    return
  fi

  POAPPIN_NAME=""
  POAPPIN_DESCRIPTION=""
  POAPPIN_HOMEPAGE=""
  POAPPIN_LICENSE=""
  POAPPIN_LICENSE_URL=""
  POAPPIN_REQUIRED_TO_RUN=""
  POAPPIN_REQUIRED_FOR_INSTALLATION=""
  POAPPIN_URL=""
  POAPPIN_FILENAME=""
  POAPPIN_VERSION=""

  . "${POAPPIN_DB_DIR}/$1.sh"
}

# Example: poappin_activate putty 0.74

function poappin_activate {
  # cmd //c mklink //J d:\\Portable\\putty d:\\poappin\\apps\\putty-0.74

  # first turn '/' into '\\', then cut and join together
  # /d/Portable/ff/gg/hh -> d:\\Portable\\ff\\gg\\hh\\
  local TODIR=`echo "${POAPPIN_CONFIG_ACTIVE_APPS_DIRECTORY}" | sed 's/\//\\\\/g' | sed -n 's/^\\\\\([a-zA-Z]\)\\\\\(.*\)$/\1:\\\\\2\\\\/p'`"$1"

  local FROMDIR=`echo "${POAPPIN_APPS_DIR}/$1-$2" | sed 's/\//\\\\/g' | sed -n 's/^\\\\\([a-zA-Z]\)\\\\\(.*\)$/\1:\\\\\2/p'`

  if [ -L "${POAPPIN_CONFIG_ACTIVE_APPS_DIRECTORY}/$1" ]; then
    echo "Removing old junction."
    cmd //c rmdir ${TODIR}
  fi

  #echo "TO: ${TODIR} FROM: ${FROMDIR}"
  cmd //c mklink //J ${TODIR} ${FROMDIR}
}

function poappin_add_icon_to_active_apps_dir {
  cat <<EOF > "${POAPPIN_CONFIG_ACTIVE_APPS_DIRECTORY}/desktop.ini"
[.ShellClassInfo]
IconResource=C:\Windows\system32\SHELL32.dll,47
[ViewState]
Mode=
Vid=
FolderType=Generic
EOF

  # attrib understands MSYS2 paths but seems to live in /c/Windows/System32?
  attrib +r +s ${POAPPIN_CONFIG_ACTIVE_APPS_DIRECTORY}
  attrib +h +s ${POAPPIN_CONFIG_ACTIVE_APPS_DIRECTORY}/desktop.ini
}

# Creates a shortcut into the start menu or to the Desktop.
#
#link.Arguments
#link.HotKey = "ALT+CTRL+F"
#link.WindowStyle 3  &&Maximized 7=Minimized  4=Normal 
#strDesktop = WshShell.SpecialFolders("Desktop")
#link = WshShell.CreateShortcut(strDesktop+"\Microsoft Web Site.URL")
#link.TargetPath = "http://www.microsoft.com"
#link.Save

# Usage: poappin_create_shortcut "Link path" "Path to exe"
# Example: poappin_create_shortcut "C:\XY\Putty.lnk" "D:\Portable\putty\PUTTY.EXE"
# TODO - unfinished code

function poappin_create_shortcut {
  local SCRIPTNAME="/tmp/poappin-create-link.vbs"
  local SCRIPTNAME_WIN=`cygpath.exe -w "${SCRIPTNAME}"`

  # use ${POAPPIN_PATH_TO_START_MENU} or "%USERPROFILE%\Desktop\"

  cat <<EOF > "${SCRIPTNAME}"
Set shell = WScript.CreateObject("WScript.Shell")
Set link = shell.CreateShortcut("d:\\Portable\\run-putty2.lnk")
link.TargetPath = "D:\\Portable\\putty\\PUTTY.EXE"
link.WorkingDirectory = "D:\\Portable"
link.IconLocation = "D:\\Portable\\putty\\PUTTY.EXE,0"
link.Description = "123"
link.Save
EOF

  cmd //c cscript.exe //nologo "${SCRIPTNAME_WIN}"
  rm -f "${SCRIPTNAME}"
}

# Some files do not provide a version number. To avoid unnecessary downloads,
# we use the last-modified header of the file. The value is mostly valid.
# It returns '24 Jun 2020', which we convert into the format '2020-06-24'.
# Example: poappin_get_last_modified_date "https://www.example.com/test.exe"

function poappin_get_last_modified_date {
  local LMD=`${POAPPIN_GET_HEADERS} ${1} 2>&1 | sed -n 's/.*Last-Modified: ..., \([0-9].*20..\) ..:..:.. GMT$/\1/p' | { read DATA ; date +"%Y-%m-%d" -d "${DATA}" ; }`

  echo "${LMD}"
}

# Reads the VERSIONINFO resource from a file using powershell. Other way:
# Extract the file and look into the text file e.g. ".rsrc\1033\version.txt":
# VALUE "ProductVersion",    "2.5   "

function poappin_get_version_info_block {
  local FILEPATH=`cygpath -w "${1}"`
  local VI=`powershell -Command "(Get-Item ${FILEPATH}).VersionInfo.ProductVersion"`

  echo "${VI}"
}

# Creates a checksums file. First cd into the apps/xx-yy package directory.

function poappin_create_checksums_here {
  rm -f checksums.txt
  find . -type f -exec sha256sum {} > ../checksums.txt \;
  mv ../checksums.txt .
}

# Creates a checksums file. Needs package name and version as parameter.
# TODO: two parameters?

function poappin_create_checksums {
  if [ ! $# -gt 0 ]; then
    echo "Usage: $FUNCNAME <full package name>"
    return
  fi

  # TODO: Check if directory exists.

  echo "Calculating checksums for all files."

  pushd "${POAPPIN_APPS_DIR}/$1" >/dev/null
  poappin_create_checksums_here
  popd >/dev/null
}

# Checks a checksums file.

function poappin_check_checksums {
  if [ ! $# -gt 0 ]; then
    echo "Usage: $FUNCNAME <package name>"
    return
  fi

  pushd "${POAPPIN_APPS_DIR}/$1" >/dev/null

  if sha256sum -c -w --quiet checksums.txt; then
    echo Files checked: okay.
  else
    echo Errors occured while file check.
  fi

  # TODO: --ignore-missing, missing or added files: own message and handling.

  popd >/dev/null
}

# Extracts a given msi file from the downloads directory into the
# current directory. Function called while installing so the
# current directory is the newly created application directory.

function poappin_extract_msi_file {
  if [ ! $# -gt 0 ]; then
    echo "Usage: $FUNCNAME <msi file name>"
    return
  fi

  # Get Windows paths and change all "/" into "\\".
  local WIN_APP_DIR=`pwd -W | sed 's/\//\\\\/g'`
  pushd ${POAPPIN_DOWNLOADS_DIR} >/dev/null
  local WIN_DL_DIR=`pwd -W | sed 's/\//\\\\/g'`
  # D:\\Poappin\\downloads
  popd >/dev/null

  echo "Executing: msiexec /a ${WIN_DL_DIR}\\$1 /qb TARGETDIR=${WIN_APP_DIR}"

  # Use /gb to see progress dialogs and /qn to hide them.
  # Please remember that users can click on the dialogs and thus cancel the unpacking.
  MSYS2_ARG_CONV_EXCL="*" \
  msiexec /a ${WIN_DL_DIR}\\$1 /qb TARGETDIR=${WIN_APP_DIR}

  sync
}


# Returns the file extension if it is contained in the list.

function poappin_get_extension {
  if [[ $1 == *.tar.gz ]]; then
    echo "tar.gz"
    return
  fi
  if [[ $1 == *.tar.bz2 ]]; then
    echo "tar.bz2"
    return
  fi
  if [[ $1 == *.exe ]]; then
    echo "exe"
    return
  fi
  if [[ $1 == *.iso ]]; then
    echo "iso"
    return
  fi
  if [[ $1 == *.tar ]]; then
    echo "tar"
    return
  fi
  if [[ $1 == *.zip ]]; then
    echo "zip"
    return
  fi
  if [[ $1 == *.msi ]]; then
    echo "msi"
    return
  fi
  if [[ $1 == *.rar ]]; then
    echo "rar"
    return
  fi
  if [[ $1 == *.xz ]]; then
    echo "xz"
    return
  fi
  if [[ $1 == *.7z ]]; then
    echo "7z"
    return
  fi
}

# Extracts a file from the ${POAPPIN_DOWNLOADS_DIR}.
# It uses the extension to select the right decompressor.

function poappin_extract_file {
  if [ ! $# == 1 ]; then
    echo "Usage: $FUNCNAME <name of downloaded file>"
    return
  fi

  if [ "$1" = "" ]; then
    echo "Error: name was empty."
    return
  fi

  if [ ! -f "${POAPPIN_DOWNLOADS_DIR}/$1" ]; then
    echo "The file \"$1\" does not exist."
    return
  fi

  local FILE_NAME="${POAPPIN_DOWNLOADS_DIR}/$1"
  local FILE_EXTENSION=`poappin_get_extension "$1"`  # ${filename##*\.}

  case "${FILE_EXTENSION}" in
    'msi')
      poappin_extract_msi_file "$1"
      ;;
    '7z' | 'exe' | 'zip' | 'rar' | 'iso' | 'xz' | 'tar')
      # -aou - aUto rename extracting file
      "${POAPPIN_PATH_TO_7ZIP}" x -aou -y "${FILE_NAME}" # > /dev/null
      ;;
    *)
      echo "Unknown archive format: \"${FILE_NAME}\"!"
      ;;
  esac
}

# Closes the bash window and shuts down all running processes.
# Use this function for a clean exit. In case you work from an USB
# medium you maybe would need the task manager to dismount it otherwise.

function poappin_quit {
  PROCESSES=`ps | tr -s ' '`
  kill -9 `echo "$PROCESSES" | grep gpg-agent | cut -d ' ' -f 2`
  kill -9 `echo "$PROCESSES" | grep ssh-agent | cut -d ' ' -f 2`

  sync

  exit
}

# Check if we use the correct shell.

if [[ "`uname -s`" == "MSYS_NT-"* ]]; then
  echo "Warning: MSYS2 bash used instead of a MinGW bash."
fi

POAPPIN_SAVED_OLD_PATH=${PATH}

poappin_find_base_dir


# Settings.

POAPPIN_APPS_DIR="${POAPPIN_BASE_DIR}/apps"
POAPPIN_DB_DIR="${POAPPIN_BASE_DIR}/database"
POAPPIN_DOWNLOADS_DIR="${POAPPIN_BASE_DIR}/downloads"
POAPPIN_BACKUPS_DIR="${POAPPIN_BASE_DIR}/backups"

POAPPIN_USE_PROXY="false"
#POAPPIN_PATH_TO_7ZIP="/c/Program Files/7-Zip/7z"
POAPPIN_PATH_TO_7ZIP="7z"
POAPPIN_LOCAL_HOST_BITS=64
POAPPIN_USERS_LANGUAGE="DE"

POAPPIN_GET_PAGE="wget --https-only --timeout=5 --tries=1 -qO-"
POAPPIN_GET_FILE="wget --https-only -c --tries=3"
POAPPIN_GET_HEADERS="wget -qS --no-http-keep-alive --spider"

# If you want to download using curl, try these options:
# curl -O -k -L -H "Cookie: x=y" "${URL}"
# Add -C - if you want the resume function,
# -L (for redirect) was essential, maybe use --verbose flag.



# Don't forget to set your proxy if there is one.
#export http_proxy='http://server:port'
#export https_proxy='https://server:port'
#git config --global http.proxy "http://server:port"
#git config --global https.proxy "https://server:port"


# Overwrites existing backup files while backing
# up an application.

POAPPIN_OVERWRITE_EXISTING_BACKUPS="true"

# Creates backup files after installing an application.

POAPPIN_CONFIG_AUTO_CREATE_BACKUPS="false"

# While restoring a backup, existing apps will be
# removed and then restored from the backup.

POAPPIN_AUTO_REMOVE_EXISTING_APPS="true"

# Activates the last installed application by removing the
# link to the existing version (if it exists) and setting
# a new one the the current version.

POAPPIN_CONFIG_AUTO_ACTIVATE="false"

# This is the directory where activated applications get
# an own sub-directory (without version number).

POAPPIN_CONFIG_ACTIVE_APPS_DIRECTORY="/c/Portable"

# Inform poappin that its not possible to reach the proxy
# server or gain direct internet access.

POAPPIN_CONFIG_OFFLINE_MODE="false"

# Create checksums, but only if they are needed/wanted.

POAPPIN_CONFIG_AUTO_CREATE_CHECKSUMS="false"

# Give the path to the start menu. Here we create the shortcuts
# to our portable applications.

# $USERPROFILE - C:\Users\user
# $SYSTEMDRIVE - C:
# $PROGRAMDATA - C:\ProgramData
#
# %SYSTEMDRIVE%\ProgramData\Microsoft\Windows\Start Menu - for all users, but not recommended:
# https://docs.microsoft.com/de-de/windows-hardware/customize/desktop/unattend/microsoft-windows-shell-setup-folderlocations-programdata
# %APPDATA%\Microsoft\Windows\Start Menu\Programs (APPDATA=%SYSTEMDRIVE%\Users\%USERNAME%\AppData\Roaming)

#POAPPIN_PATH_TO_START_MENU="/c/ProgramData/Microsoft/Windows/Start Menu/Programs/"
POAPPIN_PATH_TO_START_MENU="/c/Users/${USERNAME}/AppData/Roaming/Microsoft/Windows/Start Menu/Programs"

# Enables the creation of shortcuts in the start menu.

POAPPIN_CONFIG_AUTO_START_MENU_LINKS="false"

# Now you can overwrite all settings from above with your own configuration.
# Simply place a config.sh in poappin's base directory.

if [ -f "${POAPPIN_BASE_DIR}/config.sh" ]; then
  source "${POAPPIN_BASE_DIR}/config.sh"
fi

# Creates a backup of an in the apps directory installed application
# to the backups directory.
# TODO: two separate arguments.

function poappin_backup_create {
  if [ "$#" -ne 1 ]; then
    echo "Usage: $FUNCNAME <package name with version number>"
    return
  fi

  if [ ! -d "${POAPPIN_APPS_DIR}/${1}" ]; then
    echo "Please use '<app-name>-<version-number>' as parameter."
    echo "This combination needs to be a directory in the"
    echo "apps directory - I missed it."
    return
  fi

  if [ -x "${POAPPIN_BACKUPS_DIR}/${1}.7z" ]; then
    echo "The backup archive file already exists."
    if [ "${POAPPIN_OVERWRITE_EXISTING_BACKUPS}" = "true" ]; then
      echo "Removing the found old file."
      rm -f "${POAPPIN_BACKUPS_DIR}/${1}.7z"
    else
      echo "Please remove the backup archive and try again."
      return
    fi
  fi

  pushd ${POAPPIN_APPS_DIR} >/dev/null
  "${POAPPIN_PATH_TO_7ZIP}" a -t7z -spf -ssc -sccUTF-8 -scsUTF-8 -mx9 "${POAPPIN_BACKUPS_DIR}/${1}.7z" "${1}"
  popd >/dev/null
}

# This functions looks if the parameter is a file with .7z extension
# in the backup directory. If it is and if there is no directory with
# the same name in apps, then it extracts the backup to the apps folder.

function poappin_backup_restore {
  if [ "$#" -ne 1 ]; then
    echo "Usage: $FUNCNAME <package name with version number>"
    return
  fi

  if [ ! -f "${POAPPIN_BACKUPS_DIR}/${1}.7z" ]; then
    echo "Please use '<app-name>-<version-number>' as parameter."
    echo "Could not find the backup archive file."
    return
  fi

  if [ -d "${POAPPIN_APPS_DIR}/${1}" ]; then
    echo "This application is already installed."

    if [ "${POAPPIN_AUTO_REMOVE_EXISTING_APPS}" = "true" ]; then
      echo "Removing the already existing installed application."
      echo "This may fail, if the application is currently in use."
      rm -rf "${POAPPIN_APPS_DIR}/${1}"
    else
      echo "Please remove the application and try again."
      return
    fi
  fi

  pushd ${POAPPIN_APPS_DIR} >/dev/null
  "${POAPPIN_PATH_TO_7ZIP}" x "${POAPPIN_BACKUPS_DIR}/${1}.7z"
  popd >/dev/null
}

# Retrieves a web page and returns its data as string.
# Needs a good error handling, also timeouts. TODO.

function poappin_get_page {
  if [ $# == 0 ]; then
    echo "Usage: ${FUNCNAME[0]} <URL>"
    return
  fi

  local content=`${POAPPIN_GET_PAGE} "$1"`

  if [ $? -ne 0 ]; then
    echo "error"
  else
    echo "${content}"
  fi
}

# The command "poappin check all" checks all installed
# packages for updates.

function poappin_check_all {
  echo "Checking all packages (application name, installed version, latest version):"

  rm -f /tmp/poappin-update-needed.txt
  touch /tmp/poappin-update-needed.txt    # Else we need to check later if it exists (we cat this file).

  for path in ${POAPPIN_APPS_DIR}/*; do
    # Directory name in applications directory.
    local NAMEVERS=`echo "$path" | sed 's/.*poappin\/apps\///g'`

    # Split into application name and application version.
    local NAME=`echo "${NAMEVERS}" | sed -n 's/\(.*\)-[^-]*$/\1/p'`
    local VERS=`echo "${NAMEVERS}" | sed -n 's/.*-\([^-]*\)$/\1/p'`

    # Try extracting a date from the end of the string. If found, then replace $VERS and $NAME.
    local TST=`echo "${NAMEVERS}" | sed -n 's/.*-\(20[0-9][0-9]-[0-9][0-9]-[0-9][0-9]\)$/\1/p'`

    # If we found a date at the end of the string it is our new version number.
    if [ ! "${TST}" = "" ]; then
      VERS="${TST}"
      NAME=`echo "${NAMEVERS}" | sed -n 's/\(.*\)-20[0-9][0-9]-[0-9][0-9]-[0-9][0-9]$/\1/p'`
    fi

    poappin_load_package "${NAME}"
    poappin_app_check

    if [[ "${VERS}" == "${POAPPIN_VERSION}" ]]; then
      echo "${NAME} ${VERS} (ok)"
    else
      # If version numbers differ, only add the file to our update list
      # if the current version does not already exists as application.
      if [ -d "${POAPPIN_APPS_DIR}/${POAPPIN_NAME}-${POAPPIN_VERSION}" ]; then
        echo "${NAME} ${VERS} ${POAPPIN_VERSION} (current version already installed)"
      else
        echo "${NAME} ${VERS} ${POAPPIN_VERSION}"
        echo "${NAME}" >> /tmp/poappin-update-needed.txt
      fi
    fi
  done
}

# TODO.

function poappin_get_file {
 :
}

# Installs a package if the directory does not already exists.

function poappin_app_install_if_not_exists {
  pushd "${POAPPIN_APPS_DIR}" >/dev/null
      
  if [ -d "${POAPPIN_NAME}-${POAPPIN_VERSION}" ]; then
    echo "Package directory '${POAPPIN_NAME}-${POAPPIN_VERSION}' already exists."
    echo "Skipping installation."
  else 
    echo "Extracting into newly created directory '${POAPPIN_NAME}-${POAPPIN_VERSION}'..."
    mkdir -p "${POAPPIN_NAME}-${POAPPIN_VERSION}" && cd "${POAPPIN_NAME}-${POAPPIN_VERSION}"
    poappin_app_install
    sync
    cd ..

    if [ "${POAPPIN_CONFIG_AUTO_ACTIVATE}" = "true" ]; then
      poappin_activate "${POAPPIN_NAME}" "${POAPPIN_VERSION}"
    fi

    if [ "${POAPPIN_CONFIG_AUTO_CREATE_BACKUPS}" = "true" ]; then
      poappin_backup_create "${POAPPIN_NAME}-${POAPPIN_VERSION}"
    fi

    if [ "${POAPPIN_CONFIG_AUTO_CREATE_CHECKSUMS}" = "true" ]; then
      poappin_create_checksums "${POAPPIN_NAME}-${POAPPIN_VERSION}"
    fi
  fi

  popd >/dev/null
}

# First load the package from the database.
# Fetches the needed setup files from the homepage.
# First checks whether they have already been downloaded.

function poappin_app_fetch_if_not_exists {
  if [ "${POAPPIN_FILENAME}" == "" ]; then
    echo "No filename found. Maybe there was a timeout while retrieving the URL."
    return
  fi

  if [ -f "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" ]; then
    echo "File '${POAPPIN_FILENAME}' already exists."
  else 
    echo "File '${POAPPIN_FILENAME}' does not exist. Downloading..."
    poappin_app_fetch
  fi
}

# The central command to issue on the command line.

function poappin {
  if [ $# == 0 ]; then
    echo "Usage: ${FUNCNAME[0]} <command> <parameter>"
    echo "  check:   <app>, all, database, hashes"
    echo "  fetch:   <app>, updates, all"
    echo "  hashes:  <app>, all"
    echo "  info:    <app>, all, me"
    echo "  install: <app>"
    echo "  remove:  <app>, all"
    echo "  update:  <app>, all, database"
    return
  fi

  if [ $# == 1 ]; then
    case "$1" in
      "check")
        echo "check: <app>, all, database, hashes"
        ;;
      "fetch"|"download")
        echo "fetch: <app>, updates, all"
        ;;
      "hashes")
        echo "hashes: <app>, all"
        ;;
      "info")
        echo "info: <app>, all, me"
        ;;
      "install")
        echo "install: <app>"
        ;;
      "remove")
        echo "remove: <app>, all"
        ;;
      "update")
        echo "update: <app>, all, database"
        ;;
      *)
        echo "Unknown command: $1"
        echo ""
        echo "Usage: ${FUNCNAME[0]} <command> <parameter>"
        echo "  check:   <app>, all, database, hashes"
        echo "  fetch:   <app>, updates, all"
        echo "  hashes:  <app>, all"
        echo "  info:    <app>, all, me"
        echo "  install: <app> [<file>]"
        echo "  remove:  <app>, <app-ver>, all"
        echo "  update:  <app>, all, database"
        return
        ;;
    esac
  fi

  if [ "$1" = "check" ]; then
    # check: <app> (for updates and hashes), all (all apps for updates),
    # database (for updates), hashes (check for changed, added or missing files)

    if [ "$2" = "all" ]; then                    # "check all":
      poappin_check_all

      return
    fi

    if [ -z "$2" ]; then                         # Zero length second argument (or use the value: ${#2}).
      echo "check: No second argument given."
      return
    fi

    if [ -f "${POAPPIN_DB_DIR}/$2.sh" ]; then    # "check <app>": Check a named app (updates, missing files, hashes).
      echo "Checking package '$2' ..."
      poappin_load_package "$2"
      poappin_app_check

      if [ -f "${POAPPIN_DOWNLOADS_DIR}/${POAPPIN_FILENAME}" ]; then
        echo "Downloaded file '${POAPPIN_FILENAME}' exists."
      else 
        echo "File '${POAPPIN_FILENAME}' does not exist. Use 'fetch' to download."
      fi

      return
    fi

    echo "check (unhandled)"

    return
  elif [ "$1" = "fetch" ]; then
    if [ "$2" = "all" ]; then                    # "fetch all": Check and fetch all known apps (missing files only).
      # TODO
      return
    fi

    if [ "$2" = "updates" ]; then                # "fetch updates": Check and fetch installed apps (missing files only).
      # TODO
      return
    fi

    if [ -z "$2" ]; then                         # Zero length second argument (or use the value: ${#2}).
      echo "fetch: No second argument given."
      return
    fi

    if [ -f "${POAPPIN_DB_DIR}/$2.sh" ]; then    # "fetch <app>": Check and fetch a concrete app (missing files only).
      echo "Fetching package '$2' ..."
      poappin_load_package "$2"
      poappin_app_check
      poappin_app_fetch_if_not_exists

      return
    fi

    return
  elif [ "$1" = "hashes" ]; then
    echo "hashes"

    # hashes: <app>, all

    

    return
  elif [ "$1" = "info" ]; then
    if [ "$2" = "me" ]; then
      echo "Number of packages: `ls -1 ${POAPPIN_BASE_DIR}/database/[a-zA-Z0-9]*.sh | wc -l`"
      return
    fi

    if [ "$2" = "all" ]; then
      echo "Packages:"
      pushd ${POAPPIN_DB_DIR} >/dev/null
      echo "`ls -1 [a-zA-Z0-9]*.sh | sort | sed 's/\.sh//g' | sed 's/^/  /g'`"
      popd >/dev/null

      return
    fi

   if [ -f "${POAPPIN_DB_DIR}/$2.sh" ]; then    # "info <app>":
      poappin_load_package "$2"
      echo "Package: ${POAPPIN_NAME}"
      echo "Description: ${POAPPIN_DESCRIPTION}"
      echo "Homepage: ${POAPPIN_HOMEPAGE}"
      return
    fi

    echo "Package '$2' not found."
    return
  elif [ "$1" = "install" ]; then
    # If the parameter $2 is only a package name then first check the version
    # number and fetch the required setup files. If there is a parameter $3
    # (space separated) with a file name, which exists in the downloads
    # directory, then skip the download and install this file.

    if [ -f "${POAPPIN_DB_DIR}/$2.sh" ]; then
      echo "Installing package '$2' ..."
      poappin_load_package "$2"

      if [[ ${#3} -gt 0 ]]; then                           # File name given, and it exists.
        if [ -f "${POAPPIN_DOWNLOADS_DIR}/$3" ]; then
          export POAPPIN_FILENAME="$3"
          poappin_app_set_version
        else
          export POAPPIN_FILENAME=""
        fi
      fi

      if [ -n "${POAPPIN_FILENAME}" ]; then                # String isn't empty.
        echo "Using an existing file: '${POAPPIN_FILENAME}'."
      else
        echo "Checking for latest version."
        poappin_app_check
        echo "Found version: ${POAPPIN_VERSION}"

        poappin_app_fetch_if_not_exists
      fi

      poappin_app_install_if_not_exists

      return
    fi

    # If it is a package name with a version number, then try to recover a
    # backup with the same name.

    


    

    # Else
    
    echo "Package '$2' not found."
    return
  elif [ "$1" = "update" ]; then
    if [ "$2" = "database" ]; then               # "update database"
      echo "Updating poappin script and the database:"
      pushd ${POAPPIN_BASE_DIR} >/dev/null
      git pull --rebase
      popd >/dev/null
      return
    fi

    if [ "$2" = "all" ]; then                    # "update all"
      poappin_check_all                          # stores the needed updates in /tmp/poappin-update-needed.txt

      local PACKAGES=`cat /tmp/poappin-update-needed.txt | tr '\n' ' '`

      if [ "${PACKAGES}" = "" ]; then
        echo "No package needs an update."
      else
        echo "Updating packages: ${PACKAGES}"

        for PACKAGE in ${PACKAGES}; do
          poappin_load_package "${PACKAGE}"
          poappin_app_check

          echo "Updating package ${POAPPIN_NAME} to ${POAPPIN_VERSION}"

          poappin_app_fetch_if_not_exists
          poappin_app_install_if_not_exists
        done
      fi

      rm -f /tmp/poappin-update-needed.txt
      return
    fi

    echo "update (unhandled)"

    # update <app>

    

    return
  elif [ "$1" = "remove" ]; then
    if [ "$2" = "all" ]; then
      echo "Removing all installed applications..."
      rm -rf "${POAPPIN_APPS_DIR}/"*
      return
    fi

    local POAPPIN_PKG_NAME="$2"

    if [ -d "${POAPPIN_APPS_DIR}/${POAPPIN_PKG_NAME}" ]; then
      echo "Concrete package directory '${POAPPIN_PKG_NAME}' found. Removing..."
      rm -rf "${POAPPIN_APPS_DIR}/${POAPPIN_PKG_NAME}"
      return
    else
      echo "No concrete package directory '${POAPPIN_PKG_NAME}' found."
    fi

    # If package name is a file name in our database directory,
    # then remove all occurences (versions) of the named package.

    if [ -f "${POAPPIN_DB_DIR}/${POAPPIN_PKG_NAME}.sh" ]; then
      echo "Removing all installed packages of '${POAPPIN_PKG_NAME}'..."
      rm -rf "${POAPPIN_APPS_DIR}/${POAPPIN_PKG_NAME}-"*
      return
    else
      echo "No package named '${POAPPIN_PKG_NAME}' found."
    fi

    # Now remove the last segement (maybe it is a wrong remembered version
    # number) and look if this file name exists.

    POAPPIN_PKG_NAME=`echo "${POAPPIN_PKG_NAME}" | sed 's/-[^-]*$//g'`

    if [ -f "${POAPPIN_DB_DIR}/${POAPPIN_PKG_NAME}.sh" ]; then
      echo "Maybe you mean package '${POAPPIN_PKG_NAME}'?"
    else
      echo "Also no package '${POAPPIN_PKG_NAME}' found."
    fi
    return
  else
    echo "Unknown command: $1"
    return
  fi

}
