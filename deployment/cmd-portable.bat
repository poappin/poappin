@echo off

rem Opens a cmd window as user Portable. May be used to run free.bat from there.

rem Needs a host name or domain, not "." (but ".\Administrator") and not "localhost".
runas /noprofile /user:%computername%\Portable "cmd.exe"

