@echo off

rem Must be executed as user "Portable"!

echo Running as user '%USERNAME%'

set DIR="D:\Portable"

icacls %DIR% /grant:r Administratoren:(CI)(OI)(F)
icacls %DIR% /grant:r Benutzer:(CI)(OI)(F)

